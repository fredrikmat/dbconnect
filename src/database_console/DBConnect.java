/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database_console;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;

/**
 *
 * @author Fredrik
 */
public class DBConnect {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner scan = new Scanner(System.in);
        
        try
        {
            // Skapa databaskoppling
            String host = "jdbc:derby://localhost:1527/bilar";
            String uName = "root";
            String uPass = "root";
            Connection con = DriverManager.getConnection( host, uName, uPass );
            
            int tal;
            
            // Skriv ut meny
            System.out.println("Meny\n1: Hämta alla bilar\n2: Hämta alla personer\n3: Hämta alla bilar som har ägare\n4: Hämta personen med regnr 'ELI337'\n0: För att avsluta");
            
            do // Mata in tal så länge som det inte är 0, och lägg till i vår list
            {  
                tal = scan.nextInt();
                String SQL = null;
                
                // Välj tal, 1-4 eller 0 för exit
                switch(tal)
                {
                    case 1:
                        SQL = "SELECT * FROM bilar";
                        break;
                    case 2:
                        SQL = "SELECT * FROM person";
                        break;
                    case 3:
                        SQL = "SELECT * FROM bilar JOIN person ON person.persnr = bilar.ägare";
                        break;
                    case 4: 
                        SQL = "SELECT persnr, namn, regnr FROM person JOIN bilar ON person.persnr = bilar.ägare WHERE bilar.regnr = 'ELI337'";
                        break;
                    case 0:
                        SQL = null;
                        break;
                }
                
                if(SQL != null)
                {
                    
                    // Skapa ett statement
                    Statement stmt = con.createStatement( );
                    //Skriv ut queryn
                    System.out.println( SQL );
                    //Kör queryn
                    ResultSet rs = stmt.executeQuery( SQL );
                    // Hämta metadata för att kunna se antal resultat vi fått
                    ResultSetMetaData rsmd = rs.getMetaData();
                    
                    int antalColumns = rsmd.getColumnCount(); 
                        
                    while ( rs.next() )
                    {
                        // Loopa igenom kolumnerna
                        for(int i = 1; i <= antalColumns; i++)
                        {
                            if (i > 1) System.out.print(",  ");
                            String value = rs.getString(i); // Hämtar värdena
                            System.out.print(value);
                        }
                        System.out.println(""); // Skapa ny rad

                    }
                }
            }
            while( tal != 0 );

            
        }
        catch( SQLException ex )
        {
            System.out.println( ex.getMessage() );
        }
        
        
       
        
    }
    
}
